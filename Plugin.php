<?php namespace Sajid\Weather;

use Backend;
use System\Classes\PluginBase;

/**
 * Weather Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Weather',
            'description' => 'Provides the local weather information.',
            'author'      => 'Sajid Javed',
            'icon'        => 'icon-sun-o'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Sajid\Weather\Components\OpenWeather' => 'weather',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'sajid.weather.some_permission' => [
                'tab' => 'Weather',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'weather' => [
                'label'       => 'Weather',
                'url'         => Backend::url('sajid/weather/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['sajid.weather.*'],
                'order'       => 500,
            ],
        ];
    }
}
